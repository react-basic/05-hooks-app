import { act, renderHook } from "@testing-library/react-hooks"
import { useForm } from "../../hooks/useForm"



describe('Pruebas en useForm', () => { 


    const initialForm = {
        name: 'Christian',
        email: 'christian@gmail.com'
    }


    test('debe de regresar un formulario por defecto', () => {

        const {result} = renderHook(() => useForm(initialForm));

        expect(result.current[0]).toEqual(initialForm);
        expect(typeof result.current[1]).toBe('function');
        expect(typeof result.current[2]).toBe('function');

    })
    
    test('debe de cambiar el valor del formulario', () => {

        const {result} = renderHook(() => useForm(initialForm));

        const target = {
            name: 'name',
            value: 'Dionisio'
        }

        const [, handleInputChange] = result.current;

        act( () => {
            handleInputChange({target})
        });

        const [formValues] = result.current;

        expect(formValues).toEqual({...initialForm, name: 'Dionisio'})
        

    })

    test('debe de resetear los valores del formulario', () => {

        const {result} = renderHook(() => useForm(initialForm));

        const target = {
            name: 'name',
            value: 'Dionisio'
        }

        const [, handleInputChange, reset] = result.current;

        act( () => {
            handleInputChange({target});

            reset()
        });

        const [formValues] = result.current;

        expect(formValues).toEqual(initialForm)
        

    })

})